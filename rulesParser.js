import ImxRules from "./imxRules/ImxRules.js";
import ImxRulesGogs from "./imxRules/ImxRulesGogs.js";
import ImxRulesNamed from "./imxRules/ImxRulesNamed.js";

let useCollectionSpecificRules = [
  ImxRulesGogs
]
let keyedRules = {}

for (let rule of useCollectionSpecificRules) {
  keyedRules[rule.address] = rule
}

export let rulesParser = function(collection, namedCollections) {
  if(keyedRules[collection]) {
    return keyedRules[collection]
  }

  if(namedCollections.includes(collection)) {
    return ImxRulesNamed
  }

  return ImxRules
}