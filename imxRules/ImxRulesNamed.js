import ImxRules from "./ImxRules.js";

export default class ImxRulesNamed extends ImxRules {
  static getAllAssetNames(asset) {
    return [asset.nameId]
  }

  static getAssetMetadata(asset) {
    return JSON.stringify(
      {
        "name": [`${asset.nameId}`]
      }
    );
  }

  // some collections dont have any metadata filters enabled, we have to search by default search
  static getAssetSellTokenName(_) {
    return null
  }
}