import ImxRulesNamed from "./ImxRulesNamed.js";

export default class ImxRulesGogs extends ImxRulesNamed {
  static address = "0xee972ad3b8ac062de2e4d5e6ea4a37e36c849a11"
  static qualityNames = { // 4 being the lowest
    "Warrior ": 3,
    "Elite ": 2,
    "Mythic ": 1
  }

  static leastRareQuality = 4

  static getGogsQualityNameFromName(name) {
    for (const qualityName in ImxRulesGogs.qualityNames) {
      if(name.includes(qualityName)) {
        return qualityName
      }
    }

    return null
  }

  static parseQuality(asset) {
    try { 
      // no utility in owning skin, so we give floor to the same as common skin
      const qualityName = ImxRulesGogs.getGogsQualityNameFromName(asset.name)

      if(qualityName) return ImxRulesGogs.qualityNames[qualityName]
    } catch(err) {
      console.log(`quality parse gogs error: ${err} -- ${JSON.stringify(asset)}`)  
    }

    return ImxRulesGogs.leastRareQuality
  }

  static getLowestQualityName(asset) {
    const qualityName = ImxRulesGogs.getGogsQualityNameFromName(asset.nameId)

    if(qualityName) return asset.nameId.replace(qualityName, "");

    return super.getLowestQualityName(asset)
  }

  static getImageUrl(asset) {
    let replacedName = asset.nameId.split(" ").join("_");

    return `https://gog-art-assets.s3-ap-southeast-2.amazonaws.com/Content/Thumbnails/Heroes/${replacedName}/Thumbnail_Hero_${replacedName}_Base.png`
    //var qualityName = ImxRulesGogs.getLowestQualityName(replacedName)
    //replacedName = ImxRulesGogs.getLowestQualityName(replacedName)
    //replacedName = replacedName.split(' ').join('_');
  }
  static additionalIgnoreMetadata = [
    "type", "serialNumber", "assetId", "animation_url_mime_type", "series", "tagline", "animation_url"
  ]

  static additionalIgnoreMetadataRarity = [
    "rarityStr", "specialEditionStr"
  ]
}