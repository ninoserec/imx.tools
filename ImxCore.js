import * as ethers from "ethers"
import fetch from "node-fetch";
import { rulesParser } from "./rulesParser.js";
export default class ImxCore {
  getAssetsUrl = "https://api.x.immutable.com/v1/assets"
  getOrdersUrl = "https://api.x.immutable.com/v1/orders"

  guildOfGuardiansCollectionName = "Guild Of Guardians"

  gogsHeroesCollectionAddress = "0xee972ad3b8ac062de2e4d5e6ea4a37e36c849a11"

  maxPageSize = 199
  fetchOptions = { method: "GET", headers: {"Accept": "application/json"}}
  constructor() {
    this.useTokens = ["ETH", "GODS", "IMX", "USDC"]
    this.maxDecimalsToUse = 6
    this.tokensKeyToken = {}
    this.tokensKeyAddress = {}
    this.ratioBetween = null

    this.floorValuesNormal = {}
    this.floorApiUrl = "https://api.zkmarkets.com/"

    this.namedCollections = [this.gogsHeroesCollectionAddress]
    
    this.lastAssetUpdateTime = new Date()
  }
  async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  async fetchCryptoPrices() {
    let url = new URL(`https://min-api.cryptocompare.com/data/price?fsym=USD&tsyms=${  this.useTokens.toString()}`)

    await fetch(url, this.fetchOptions).then(response => response.json())
      .then(response => {
        this.ratioBetween = response
      }).catch((err) => {
        console.log("fetchCryptoPrices", err)
      })
  }
  async getImxTokens() {
    let url = new URL("https://api.x.immutable.com/v1/tokens")

    await fetch(url, this.fetchOptions).then(response => response.json())
      .then(response => {
        let tokensTmp = response.result

        for(let i = 0; i<tokensTmp.length; i++) {
          let currentToken = tokensTmp[i]

          currentToken.decimals = parseInt(currentToken.decimals)
          this.tokensKeyToken[currentToken.symbol] = currentToken
          this.tokensKeyAddress[currentToken.token_address] = currentToken
        }
      }).catch(async (err)  => {
        console.log("getImxTokens sleeping...", err)
        await this.sleep(120000);
        await this.getImxTokens()
      })
  }
  async fetchFloors(collectionAddress) {
    let url = new URL(`${this.floorApiUrl}floors/${collectionAddress}`)

    try {
      const responseRaw = await fetch(url, this.fetchOptions)
      const response = await responseRaw.json()

      this.floorValuesNormal = {[collectionAddress]: response}
    } catch(err) {
      console.log("fetchFloors", err)
    }
  }
  async fetchOrders(address = "https://api.x.immutable.com", status = "active", only = null) {
    let url = new URL(`${address}/v1/orders`)
    let params = {
      order_by: "updated_at",
      page_size: this.maxPageSize
    }

    if (status) params.status = status

    if (this.lastAssetUpdateTime) params.updated_min_timestamp = this.lastAssetUpdateTime.toISOString()

    if (only) params.sell_token_address = only

    url.search = new URLSearchParams(params).toString();
    let responseRaw = null

    responseRaw = await fetch(url, this.fetchOptions)
    const response = await responseRaw.json()

    return response.result
  }
  convertNormalTokenValueToUsdValue(normalPrice, tokenSymbol) {
    return (normalPrice / this.ratioBetween[tokenSymbol]).toFixed(this.maxDecimalsToUse)
  }
  convertBigNumberStringToNormalValue(bigVal, tokenSymbol) {
    try {
      return ethers.utils.formatUnits(bigVal, this.tokensKeyToken[tokenSymbol].decimals)
    } catch(err) {
      console.log(`${bigVal} ${tokenSymbol} ${this.tokensKeyToken}`);
      throw err;
    }
  }
  formatAsset (order, canSkipPostFormat = false) {
    let formattedAsset = null
 
    try {
      if(!order || !order.buy) return order

      let orderERC721; let orderCoin = null

      if(order.buy.type === "ERC721") {
        orderERC721 = order.buy
        orderCoin = order.sell
      } else {
        orderERC721 = order.sell
        orderCoin = order.buy
      }

      formattedAsset = {
        orderId: order.order_id,
        order,
        orderERC721,
        orderCoin,
        
        name: orderERC721.data.properties.name,
        image_url: orderERC721.data.properties.image_url,
          
        token_address: orderERC721.data.token_address,
        collection: orderERC721.data.token_address,
        token_id: orderERC721.data.token_id,
          
        updated_timestamp: order.updated_timestamp,
      }

      formattedAsset.rules = rulesParser(formattedAsset.collection, [])
      formattedAsset.buy_token_symbol = this.parseBuyTokenSymbol(orderCoin)

      if(!formattedAsset.buy_token_symbol) return null

      formattedAsset.price = this.convertBigNumberStringToNormalValue(orderCoin.data.quantity, formattedAsset.buy_token_symbol)
      formattedAsset.quality = formattedAsset.rules.parseQuality(formattedAsset)
      formattedAsset.nameId = formattedAsset.rules.getAssetNameId(formattedAsset)
      formattedAsset.priceUSD = this.convertNormalTokenValueToUsdValue(formattedAsset.price, formattedAsset.buy_token_symbol)
    } catch(err) {
      console.log(`preformat err: ${err.stack}`)

      return null
    }

    try {
      formattedAsset.floorValue = this.getFloorValue(formattedAsset, this.floorValuesNormal)
    } catch(err) {
      console.log(`format err: ${formattedAsset.nameId}`, err)

      return canSkipPostFormat ? formattedAsset : null
    }

    return formattedAsset
  }
  getFloorValue(asset, floorValues) {
    let collection = asset.token_address
    let collectionFloors = floorValues[collection]

    if (!collectionFloors) {
      throw (`floor - no collection!!!! ${collection}`)
    }

    let nameId = asset.nameId
    let assetFloors =  collectionFloors[nameId]

    return assetFloors
  }

  updateLastAssetUpdateTime(timestamp) {
    this.lastAssetUpdateTime = new Date(timestamp) 
    this.lastAssetUpdateTime.setMilliseconds(this.lastAssetUpdateTime.getMilliseconds() + 1)
  }

  parseBuyTokenSymbol(orderCoin) {
    return orderCoin.type === "ETH" ? "ETH" : this.tokensKeyAddress[orderCoin.data.token_address]?.symbol
  }

  //scrape all assets in collection, keyed by nameId
  async fetchCollectionAssetsRecursive (collection, cursor = "", assetsKeyNameId = {}) {
    return new Promise(async (res, rej) => {
      try {
        let url = new URL(this.getAssetsUrl)
        let params = {
          order_by: "nameId",
          direction: "asc",
          collection,
          cursor,
          page_size: this.maxPageSize
        }

        url.search = new URLSearchParams(params).toString();
        const response = await fetch(url, this.fetchOptions)
        const responseData = await response.json()
        let newAssets = responseData.result
        
        for (let asset of newAssets) {
          asset.rules = rulesParser(collection, this.namedCollections)
          asset.quality = asset.rules.parseQuality(asset)
          asset.nameId = asset.rules.getAssetNameId(asset)

          if(assetsKeyNameId[asset.nameId]) {
            assetsKeyNameId[asset.nameId].amount++
          } else {
            assetsKeyNameId[asset.nameId] = {
              nameId: asset.nameId,
              amount: 1,
              metadata: asset.metadata
            }
          }
        }

        if (newAssets.length > 0) {
          await this.sleep(200)
          console.log("scraping...")
          res(await this.fetchCollectionAssetsRecursive(collection, responseData.cursor, assetsKeyNameId))
        } else {
          res(assetsKeyNameId)
        }
      } catch(err) {
        rej(new Error(`err fetchAssetRecursive ${err.stack}`))
      }
    });
  }
}
