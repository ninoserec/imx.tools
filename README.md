# imx.tools
Analytics and rarity for "Named collections" on imx. Here we will explain how to scrape data from immutable API and follow the immutable Orderbook without getting rate limited.
We will also provide open API for asset floor values and other data and then use this data in example discord listing bot that follows the immutable Orderbook.

### Immutable API rate limits
Check out our take on rate limits [here](https://docs.google.com/document/d/1NvTaBfnpnuGrdC0rTVrxvO_X8mZIgVmslvKjIOTv37M).

# zkmarket open API
Using the scraping methods and following the immutable Orderbook live, we created our own up to date open API for statistics and floor data for named collections like Gods Unchained and Guild of Guardians. Usually this would require thousands of API calls to get all the data, one for each buy token symbol and repeat for every different asset. With this API you can get all that data with just one call. The API url parameters are collection address and or target asset name.

### Base url
`https://api.zkmarkets.com`

## Get a list of available collections
`GET /collections/`

Response format:
```
[
  {
    "address": "0xacb3c6a43d15b907e8433077b6d38ae40936fe2c",
    "name": "Gods Unchained"
  },
  {
    "address": "0xee972ad3b8ac062de2e4d5e6ea4a37e36c849a11",
    "name": "Guild of Guardians Heroes"
  },
  ...
]
```

## Get collection filterable metadata.
`GET /collections/:collection_address`

Response format:
```
{
  "attributes": {
    "class": {
      "Melee": 62,
      "Mage": 37,
      "Ranged": 19
    },
    "element": {
      "Light": 28,
      "Dark": 22,
      "Fire": 24,
      "Water": 23,
      "Earth": 21
    },
    ...
  },
  "assetCount": 192213
}
```

Metadata values represent how many unique assets (in named collections, assets with same name are not considered unique to each other) share that trait.

## Get floor values
`GET /floors/:collection_address`

Response format:
```
{
  "Akhun": {
    "ETH": 0.004982,
    "USDC": 41.34,
    "GODS": 23.94,
    "IMX": 16.96
  },
  "Aria": {
    "ETH": 0.005618,
    "USDC": 29.68,
    "GODS": 31.8,
    "IMX": 15.9
  },
  ...
}
```

## Get collection assets
`GET /assets/:collection_address`

asset floor data with metadata.

Response format:
```
{
  "Akhun": {
    "nameId": "Akhun",
    "amount": 7838,
    "floorData": {
      "listings": 199,
      "ETH": 0.004982,
      "USDC": 41.34,
      "GODS": 23.94,
      "IMX": 16.96
    },
    "metadata": {
      "name": "Akhun",
      "type": 24,
      "class": "Melee",
      "series": "Founder",
      "assetId": "618d376bf3a98600165b090b",
      "element": "Light",
      "faction": "Empire",
      "tagline": "Prince of Light",
      "image_url": "https://gog-art-assets.s3-ap-southeast-2.amazonaws.com/Content/Thumbnails/Heroes/Akhun/Thumbnail_Hero_Akhun_Base.png",
      "rarityStr": "Rare",
      "serialNumber": "1065",
      "animation_url": "https://guildofguardians.mypinata.cloud/ipfs/QmTCZf3Qc3n7N66ceCuHC6dXZaRubNzChEeu538mtCPvVi/HLS/Base/CollectionAsset_Hero_Akhun_Base.m3u8",
      "specialEditionStr": "Normal",
      "animation_url_mime_type": "application/vnd.apple.mpegurl"
    }
  },
  ...
}
```

Requests here have to be shaped by the page and page_size parameters.
Additionally we can add filters to the request.

**Parameters**

| Name        | Type          | Description  |
| :-------------: |:-------------:| :-----:|
| ```page``` | ![number](https://img.shields.io/badge/-address-green.svg) | page for pagination (default 1) |
| ```page_size``` | ![number](https://img.shields.io/badge/-Rarity-green.svg) | page size for pagination (default 20) |
| ```name_id_filter``` | ![string](https://img.shields.io/badge/-Rarity-blue.svg) | nameId filter |
| ```metadata``` | ![object](https://img.shields.io/badge/-Rarity-blue.svg) | object keyed by attribute type (value is array of values)  |

Example:
```
https://api.zkmarkets.com/assets/0xee972ad3b8ac062de2e4d5e6ea4a37e36c849a11?name_id_filter=ik&metadata={"class":["Mage","Melee"]}
```

`GET /assets/:collection_address/:asset_name_id`
Get collection asset data.

Example:
```
https://api.zkmarkets.com/assets/0xee972ad3b8ac062de2e4d5e6ea4a37e36c849a11/Akhun
```

# Scraping collection on immutable
In this section we will explain how we scrape collection data. For this example we will be calculating rarity and will be using standard NFT rarity ranking (rarity score). To find out more about different rarity methods read more on this article https://raritytools.medium.com/ranking-rarity-understanding-rarity-calculation-methods-86ceaeb9b98c.

### Rarity score

```
[Rarity Score for a Trait Value] = 1 / ([Number of Items with that Trait Value] / [Total Number of Items in Collection])
The total Rarity Score for an NFT is the sum of the Rarity Score of all of it’s trait values.
```

**Note** we have to ignore some metadata like image_url, name etc...

### node bin/scrapeCollection :collection_address
Example
```
node bin/scrapeCollection 0x5f32923175e13713242b3ddd632bdee82ab5f509
```

Response format for assets:
```
"#1": {
    "nameId": "#1",
    "amount": 1,
    "metadata": {
        "Hat": "Angel Halo",
        "Face": "Aviator Glasses",
        "name": "#1",
        "Clothes": "None",
        "Krow Base": "The Diamond Krow",
        "image_url": "https://ipfs.io/ipfs/QmYtvx8qARkkBUS7U3NK7z1BnDnVR22XczsiQB5R6VM7CJ",
        "Accessories": "Diamond Earring",
        "Backgrounds": "White"
    },
    "qualityScore": 0.00020102577043187327, // this can be multiplied by a custom factor (10^6) to get a nicer score
    "rank": 1
},
...
```
Additionaly we save some collection data and amount of assets that contain a specific attribute type and value:
```
    {
        "Backgrounds": {
            "White": 495,
            "Dark Blue": 522,
            "Dark Purple": 473,
            "Neon Orange": 542,
            "Pink": 469,
            "Green": 541,
            "Violet": 501,
            "Bright Gold": 465,
            "Brown": 513,
            "Hot Pink": 472,
            "Maroon": 512,
            "Dark Green": 461,
            "Red": 512,
            "Yellow": 512,
            "Black": 515,
            "Bright Red": 508,
            "Purple": 518,
            "Blue": 492,
            "Dark Orange": 496,
            "Neon Green": 481
        },
        ...
    },
    "assetCount": 10000
```

# Following immutable Orderbook
In "ExampleServer.js" we can find usage of "ExampleBot.js" that uses zkmarket open API to list only GoG hero assets that listed below their current floor. The ExampleServer can be run using node.js. Don't forget to set .env variables.

### .env
```
DISCORD_BOT_LOGIN_KEY=myKey
LISTING_CHANNEL_ID=myChannelId
```

# Whats next?
We think this API is very useful for new developers and will help lots of new coming projects! We are currently building a marketplace that is using this API (https://zkmarkets.com/). For any questions visit us on our imx discord (https://discord.gg/vfauZtPcjg)