import Discord from "discord.js";
import DiscordBot from "./DiscordBot.js";
export default class UtilsBot extends DiscordBot {
  async onMessageCreate(message) {
    let prefix = process.env.DISCORD_BOT_COMMAND_PREFIX ?? ":"
    const args = message.content.substring(prefix.length).split(/ +/);

    if(!message.content.startsWith(prefix)) return; // should start with command prefix

    if(!message.member.permissions.has("ADMINISTRATOR")) return;

    console.log(`${message.member} called command: ${message.content}`)

    switch(args[0]){
    case "test":
      this.testList(message.channel)

      if(args[1] && args[2]) {
        //this.testList(message.channel, args[2])
      }

      break;
    }
  }
  listNewOrder(channelId, asset, largeImage = false, type = "active") {  // types are active and filled
    try {
      const channelObject = this.discordClient.channels.cache.get(channelId);

      console.log("listing...")
      let url; let baseDescription

      if(type === "active") {
        baseDescription = "New active order for "
        url = `https://zkmarkets.com/assets/${asset.token_address}/${encodeURIComponent(asset.nameId)}`;
      } else {
        baseDescription = "New filled order for "
        url = `http://immutascan.io/address/${asset.token_address}/${asset.orderERC721.data.token_id}`;
      }

      const assetEmbed = new Discord.EmbedBuilder();
      let floorValueText = asset.floorValue ? `${asset.floorValue[asset.buy_token_symbol]} ${asset.buy_token_symbol} (${this.core.convertNormalTokenValueToUsdValue(asset.floorValue[asset.buy_token_symbol], asset.buy_token_symbol)}$) \n` : "?"

      let description = `${baseDescription} ${asset.nameId}\n
      for ${asset.price} ${asset.buy_token_symbol} (${asset.priceUSD}$)
      floor ${floorValueText}`
      
      assetEmbed.setColor("#0099ff")
        .setTitle(asset.nameId)
        .setURL(url)
        .setDescription(description);

      if(largeImage) {
        assetEmbed.setImage(asset.orderERC721.data.properties.image_url);
      } else {
        assetEmbed.setThumbnail(asset.orderERC721.data.properties.image_url);
      }

      if(channelObject.permissionsFor(this.discordClient.user).has(Discord.PermissionsBitField.Flags.SendMessages)) {
        channelObject.send({embeds: [assetEmbed]});
      } else {
        console.log("no permissions in")
        console.log(channelObject)
      }
    } catch (err) {
      console.log("error sending", err.stack)
    }
  }
}